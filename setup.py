from setuptools import find_packages
from setuptools import setup


setup(
    name='bme280',
    use_scm_version=True,
    package_dir={'': 'src'},
    packages=find_packages('src'),
    version='1.0.0',
    description='Python library for reading BME280 sensor using Yocto-I2C',
    install_requires=[
        'yoctopuce',
    ],
    maintainer='Guzmán Borque Gallego',
    maintainer_email='gbg@csem.ch',
    python_requires='>=3',
    license='MIT'
)
