# BME280 Sensor

Python library to read BME280 environmental sensor using Yocto-I2C USB interface.

This package is a refactoring of [bme280-python](https://github.com/pimoroni/bme280-python) and [i2cdevice-python](https://github.com/pimoroni/i2cdevice-python) packages using [Yoctopuce API](https://github.com/yoctopuce/yoctolib_python) instead of SMBus.

## Requirement

The device connection requires yoctopuce virtualhub, in order to be able to use several yoctopuces on the same computer.

```shell
sudo apt-get install virtualhub 
sudo VirtualHub --install_udev_rule 
sudo VirtualHub -I 
```

## Installation

The `bme280` package can be installed with `pip` after cloning the repository.

```shell
git clone https://gitlab.csem.local/div-e/project/triage/bme280-lib.git
cd bme280-lib

# Optional but strongly recommended.
virtualenv venv
source venv/bin/activate  # or venv\Scripts\activate on Windows.

pip install --editable .
```

## Usage

The package provides a simple interface to read temperature, pressure and humidity.
